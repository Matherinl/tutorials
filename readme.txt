This repository is used to store various language's and framework's tutorials. It will contain practice 
materials and notes for each technology used in the interest of using what I learn as a reference in the 
future. Any applications made using these technologies will be in their own repositories unless they are 
proof of concept applications.
